/* Overlay.js */
(function(exports)
{
	'use strict';
	
	
	function Overlay()
	{
		this.eventAdd();
	}
	
	
	
	Overlay.prototype.element = null;
	
	
	
	Overlay.prototype.eventAdd = function()
	{
		if (this.element === null)
		{
			var _this = this,
				callbackKey = function(ev)
				{
					_this.key(ev.keyCode);
				};
			this.element = document.getElementById('overlay');
			this.element.addEventListener('click', function(ev)
			{
				_this.close();
				ev.stopPropagation();
			});
			
			document.addEventListener('keyup', callbackKey);
		}
	};
	
	
	Overlay.prototype.show = function(show, name)
	{
		var elementName;
		this.element.classList.remove('winner');
		this.element.classList.remove('looser');
		this.element.classList.remove('draw');
		
		if (show)
		{
			this.element.hidden = false;
			elementName = document.querySelector('#overlay .winner .name');
			if (typeof name === 'string' && name !== '')
			{
				elementName.textContent = ' ' + name;
			}
			else
			{
				elementName.textContent = '';
			}
			this.element.classList.add(show);
			
			this.element.dispatchEvent(new CustomEvent('show', {
				'detail': {
					'show': show,
					'name': name,
				}
			}));
		}
		else
		{
			this.element.hidden = true;
		}
	};
	
	
	Overlay.prototype.key = function(keyCode)
	{
		switch (keyCode)
		{
			case 27: // escape
			case 13: // enter
				if (!this.element.hidden)
				{
					this.close();
				}
			break;
		}
	};
	
	
	Overlay.prototype.close = function()
	{
		this.show();
		this.element.dispatchEvent(new CustomEvent('close'));
		// this.element.dispatchEvent(this.event.close);
	};
	
	
	
	exports.Overlay = Overlay;
})(self);
