/* TicTacToe.js */
(function(exports)
{
	"use strict";
	
	function TicTacToe(rootElement)
	{
		this.rootElement = rootElement;
		this.player = !Math.round(Math.random());
		this.overlay = new Overlay();
		this.artificialIntelligence = new ArtificialIntelligence(this);
	}
	
	
	
	TicTacToe.init = function()
	{
		var game = new TicTacToe(document.getElementById('TicTacToe'));
		
		game.run();
		
		window.game = game; // debug
	};
	
	
	
	TicTacToe.prototype.rootElement = document.documentElement;
	
	
	TicTacToe.prototype.player = true;
	
	
	TicTacToe.prototype.level = 'medium';
	
	
	
	TicTacToe.prototype.run = function()
	{
		this.eventAdd();
		this.reset();
	};
	
	
	TicTacToe.prototype.reset = function()
	{
		if (location.hash.length > 1)
		{
			this.level = location.hash.substr(1);
		}
		else
		{
			this.level = 'medium';
		}
		location.hash = this.level;
		this.rootElement.reset();
		this.playTurn();
	};
	
	
	TicTacToe.prototype.eventAdd = function()
	{
		var i,
			_this = this,
			l = this.rootElement.querySelectorAll('td'),
			callbackClick = function(ev)
			{
				_this.click(ev.target);
			},
			callbackKey = function(ev)
			{
				_this.key(ev.keyCode);
			},
			callbackClose = function(ev)
			{
				_this.reset();
			};
		for (i = 0; i < l.length; i++)
		{
			l[i].addEventListener('click', callbackClick);
		}
		
		document.addEventListener('keyup', callbackKey);
		// document.addEventListener('keypress', callbackKey);
		
		this.overlay.element.addEventListener('close', callbackClose);
	};
	
	
	// retourne une matrice (un tableau de tableau JS) avec une valeur dans chaque case : null : la case n'est pas cochée, true : case avec une croix, false : case avec un rond le résultat de la fonction sera un truc du genre :
	/*
	exemple
	[
		[ null, true, null ],
		[ false, true, null ],
		[ null, true, false ],
	]
	*/
	TicTacToe.prototype.print = function(message)
	{
		// document.getElementById('output').innerHTML = JSON.stringify(message);
	};
	
	
	TicTacToe.prototype.getMap = function()
	{
		var matrice = new Array(3), line, i, j;
		// pour chaque ligne
		for (i = 0; i < 3; i = i + 1)
		{
			line = new Array(3);
			// pour chaque cellule de la ligne
			for (j = 0; j < 3; j = j + 1)
			{
				line[j] = this.getChecked('cell_' + i + '_' + j);
			}
			matrice[i] = line;
		}
		this.print(matrice);
		
		this.matrice = matrice;
	};
	
	
	// retourne la valeur de la cellule de données en paramètres
	TicTacToe.prototype.getChecked = function(name)
	{
		// recuperer dans l'html les 'name' qui sont pareils
		var cellList = this.rootElement.querySelectorAll('[name="' + name + '"]');
		
		if (cellList[0].checked)
		{
			return 'O';
		}
		else if (cellList[1].checked)
		{
			return 'X';
		}
		else
		{
			return null;
		}
	};
	
	
	// vérifie s'il y a une ligne gagnante, si oui, par quel joueur
	TicTacToe.prototype.checkLine = function()
	{
		var i, line;
		for (i = 0; i < 3; i = i + 1)
		{
			line = this.matrice[i];
			// si ligne 0 = ligne 1 = ligne 2
			if (line[0] === line[1] && line[0] === line[2] && line[0] !== null)
			{
				return line[0];
			}
		}
		return null;
	};
	
	
	// vérifie s'il y a une colonne gagnante, si oui, par quel joueur
	TicTacToe.prototype.checkColumn = function()
	{
		var i, value,
			l = this.matrice.length,
			reduceCallback = function(previousValue, currentValue, index, array)
			{
				if (previousValue === currentValue)
				{
					return currentValue;
				}
				else
				{
					return null;
				}
			};
		
		for (i = 0; i < l; i++)
		{
			value = this.getColumn(i).reduce(reduceCallback);
			
			if (value !== null)
			{
				return value;
			}
		}
		
		return null;
	};
	
	
	// vérifie s'il y a une diagonale gagnante, si oui, par quel joueur
	TicTacToe.prototype.checkDiagonal = function()
	{
		var i, cell, prev, isTheSame;
		for (i = 0; i < 3; i = i + 1)
		{
			isTheSame = true;
			
			cell = this.matrice[i][i];
			if (i !== 0 && cell !== prev)
			{
				isTheSame = false;
				// stop
				break;
			}
			prev = cell;
		}
		
		if (isTheSame === true && prev !== null)
		{
			return prev;
		}
		
		for (i = 0; i < 3; i = i + 1)
		{
			isTheSame = true;
			
			cell = this.matrice[i][2 - i];
			if (i !== 0 && cell !== prev)
			{
				isTheSame = false;
				// stop
				break;
			}
			prev = cell;
		}
		
		if (isTheSame === true && prev !== null)
		{
			return prev;
		}
		
		return null;
	};
	
	
	TicTacToe.prototype.getLine = function(lineNumber)
	{
		if (0 <= lineNumber && lineNumber < this.matrice.length)
		{
			return this.matrice[lineNumber];
		}
		else
		{
			throw new RangeError(lineNumber + " must be beetween 0 and " + (this.matrice.length - 1));
		}
	};
	
	
	TicTacToe.prototype.getColumn = function(columnNumber)
	{
		if (0 > columnNumber || columnNumber >= this.matrice[0].length)
		{
			throw new RangeError(columnNumber + " must be beetween 0 and " + (this.matrice.length - 1));
		}
		
		var i, l,
			column = new Array(3);
		for (i = 0, l = this.matrice.length; i < l; i = i + 1)
		{
			column[i] = this.matrice[i][columnNumber];
		}
		return column;
	};
	
	
	TicTacToe.prototype.getDiagonal = function(diagonalNumber)
	{
		var i, l, diagonal = new Array(3);
		
		if (diagonalNumber === 0)
		{
			for (i = 0, l = this.matrice.length; i < l; i = i + 1)
			{
				diagonal[i] = this.matrice[i][i];
			}
		}
		else if (diagonalNumber === 1)
		{
			for (i = 0, l = this.matrice.length; i < l; i = i + 1)
			{
				diagonal[i] = this.matrice[i][2 - i];
			}
		}
		else
		{
			throw new RangeError(diagonalNumber + " must be beetween 0 and 1");
		}
		
		return diagonal;
	};
	
	
	// vérifie s'il y a un gagnant, si oui, lequel (execute
	TicTacToe.prototype.checkWinner = function()
	{
		return this.checkLine() || this.checkColumn() || this.checkDiagonal();
	};
	
	
	TicTacToe.prototype.isFinish = function()
	{
		var line, i, j;
		
		for (i = 0; i < 3; i = i + 1)
		{
			line = this.matrice[i];
			
			for (j = 0; j < 3; j = j + 1)
			{
				if (line[j] === null)
				{
					return false;
				}
			}
		}
		
		return true;
	};
	
	
	TicTacToe.prototype.click = function(el)
	{
		var inputPlayer = el.querySelector('input[value="' + (0 + this.player) + '"]'),
		    inputIA = el.querySelector('input[value="' + ((1 + this.player) % 2) + '"]');
		if (inputPlayer && !inputPlayer.checked && inputIA && !inputIA.checked && this.player)
		{
			inputPlayer.checked = true;
			this.playTurn();
		}
	};
	
	
	TicTacToe.prototype.key = function(keyCode)
	{
		var i, j;
		
		switch (keyCode)
		{
			// 1
			case 97:
				i = 2;
				j = 0;
			break;
			
			// 2
			case 98:
				i = 2;
				j = 1;
			break;
			
			// 3
			case 99:
				i = 2;
				j = 2;
			break;
			
			// 4
			case 100:
				i = 1;
				j = 0;
			break;
			
			// 5
			case 101:
				i = 1;
				j = 1;
			break;
			
			// 6
			case 102:
				i = 1;
				j = 2;
			break;
			
			// 7
			case 103:
				i = 0;
				j = 0;
			break;
			
			// 8
			case 104:
				i = 0;
				j = 1;
			break;
			
			// 9
			case 105:
				i = 0;
				j = 2;
			break;
			
			default:
				return;
		}
		
		
		var input = this.rootElement.querySelector('#cell_' + i + '_' + j + '_' + (0 + this.player));
		this.getMap();
		// if (input !== null && this.matrice[i][j] === null)
		if (input !== null && this.player && this.matrice[i][j] === null)
		{
			input.checked = true;
			this.playTurn();
		}
	};
	
	
	TicTacToe.prototype.saveStat = function(winnerName)
	{
		var stat = {
				winner: {}
			};
		
		// si le nom est vide on le remplace par anonymous
		if (winnerName === null || winnerName === '')
		{
			winnerName = 'anonymous';
		}
		
		// récupère les anciens stats
		if (typeof localStorage['TicTacToe'] !== 'undefined')
		{
			stat = JSON.parse(localStorage['TicTacToe']);
		}
		
		if (typeof stat.winner[winnerName] === 'undefined')
		{
			stat.winner[winnerName] = 1;
		}
		else
		{
			stat.winner[winnerName]++;
		}
		
		// enregistre les stats mis à jour
		localStorage['TicTacToe'] = JSON.stringify(stat);
	};
	
	
	/*
	playTurn() // execute un tour (c'est du tour par tour)
		vérifie si il y a un gagnant
			si oui
				vérifie si c'est le joueur
				si oui
					appel winner()
				sinon
					appel looser()
			appel saveStat()
		vérifie si la partie est finie
			si oui
				appel draw()
				appel saveStat()
		défini le joueur actuel (inverse l'actuel)
		si le joueur actuel est l'IA
			alors
				appel AI()
		sinon
			ajouter les evenements de click sur le jeu
	*/
	TicTacToe.prototype.playTurn = function()
	{
		this.getMap();
		
		var winnerName,
			winnerPlayer = this.checkWinner();
		
		if (winnerPlayer !== null)
		{
			if (winnerPlayer === 'X')
			{
				winnerName = prompt('Name of the winner : ');
				if (winnerName !== null)
				{
					winnerName = winnerName.replace(/^\s*|\s*$/, '');
				}
				else
				{
					winnerName = '';
				}
				this.overlay.show('winner', winnerName);
			}
			else
			{
				winnerName = 'artificial intelligence';
				this.overlay.show('looser');
			}
			this.saveStat(winnerName);
		}
		else if (this.isFinish())
		{
			this.saveStat('draw');
			this.overlay.show('draw');
		}
		else
		{
			this.player = !this.player;
			
			if (!this.player)
			{
				this.artificialIntelligence[this.level]();
			}
		}
	};
	
	
	
	exports.TicTacToe = TicTacToe;
})(self);

document.addEventListener('DOMContentLoaded', TicTacToe.init);
