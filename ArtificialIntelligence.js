/* ArtificialIntelligence.js */
(function(exports)
{
	"use strict";
	
	function randomFloat(start, end)
	{
		if (typeof end !== "number")
		{
			end = start;
			start = 0;
			
			if (typeof end !== "number")
			{
				end = 1;
			}
		}
		return start + ((end - start) * Math.random());
	}
	exports.randomFloat = randomFloat;
	
	
	function randomInt(start, end)
	{
		return Math.round(randomFloat(start, end));
	}
	exports.randomInt = randomInt;
	
	
	function randomBool(ratio)
	{
		if (typeof ratio !== "number")
		{
			ratio = 0.5;
		}
		return !!Math.round(0.5 + ratio - Math.random());
	}
	exports.randomBool = randomBool;
	
	
	
	function ArtificialIntelligence(game)
	{
		this.game = game;
	}
	
	
	
	ArtificialIntelligence.prototype.game = null;
	
	
	
	ArtificialIntelligence.prototype.easy = function()
	{
		// console.debug('easy');
		var i, j, input;
		
		do
		{
			i = randomInt(0, 2);
			j = randomInt(0, 2);
		}
		while (this.game.matrice[i][j] !== null);
		
		input = this.game.rootElement.querySelector('#cell_' + i + '_' + j + '_' + (0 + this.game.player));
		
		input.checked = true;
		
		this.game.playTurn();
	};
	
	
	ArtificialIntelligence.prototype.medium = function()
	{
		if (randomBool(0.8))
		{
			// console.debug('hard');
			this.hard();
		}
		else
		{
			// console.debug('easy');
			this.easy();
		}
	};
	
	
	ArtificialIntelligence.prototype.hard = function()
	{
		var i, j, k, tupple,
			checked = false,
			game = this.game,
			checkPart = [2, -2],
			l = game.matrice.length,
			reduceCallback = function(previousValue, currentValue, index, array)
			{
				if (currentValue === "X")
				{
					previousValue--;
				}
				else if (currentValue === "O")
				{
					previousValue++;
				}
				
				return previousValue;
			},
			check = function(i, j)
			{
				if (!checked)
				{
					var input = game.rootElement.querySelector('#cell_' + i + '_' + j + '_' + (0 + game.player));
					if (input !== null)
					{
						input.checked = true;
						checked = true;
					}
				}
			};
		
		try
		{
			for (k = 0; !checked && k < 2; k++)
			{
				for (i = 0; !checked && i < l; i++)
				{
					tupple = game.getLine(i);
					
					if (tupple.reduce(reduceCallback, 0) === checkPart[k])
					{
						for (j = 0; j < l; j++)
						{
							if (tupple[j] === null)
							{
								check(i, j);
								break;
							}
						}
					}
					
					tupple = game.getColumn(i);
					
					if (!checked && tupple.reduce(reduceCallback, 0) === checkPart[k])
					{
						for (j = 0; j < l; j++)
						{
							if (tupple[j] === null)
							{
								check(j, i);
								break;
							}
						}
					}
					
					if (!checked && i < 2)
					{
						tupple = game.getDiagonal(i);
						
						if (tupple.reduce(reduceCallback, 0) === checkPart[k])
						{
							if (i === 0)
							{
								for (j = 0; j < l; j++)
								{
									if (tupple[j] === null)
									{
										check(j, j);
										break;
									}
								}
							}
							else
							{
								for (j = 0; j < l; j++)
								{
									if (tupple[j] === null)
									{
										check(j, 2 - j);
										break;
									}
								}
							}
						}
					}
				}
			}
			
			if (checked)
			{
				// console.debug('hard hard');
				game.playTurn();
			}
			else
			{
				// console.debug('hard easy');
				game.artificialIntelligence.easy();
			}
		}
		catch (e)
		{
			console.debug(e);
			// console.debug('hard easy');
			game.artificialIntelligence.easy();
		}
	};
	
	
	
	exports.ArtificialIntelligence = ArtificialIntelligence;
})(self);
